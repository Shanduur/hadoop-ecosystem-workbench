#!/usr/bin/env bash

[[ -f "data" ]] || mkdir data

curl -sSL https://docs.aws.amazon.com/emr/latest/ManagementGuide/samples/food_establishment_data.zip > data/food_establishment_data.zip
